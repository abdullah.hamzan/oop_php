<?php 

require_once('animal.php');
require_once("ape.php");
require_once("frog.php");


$sheep = new Animal("shaun");

echo "name:".$sheep->name."<br>"; // "shaun"
echo "legs:".$sheep->legs."<br>";// 4
echo "cold_blooded:".$sheep->cold_blooded."<br>";// "no"
echo "<br>";

$kodok = new Frog("buduk");
echo "name:".$kodok->name."<br>"; // "buduk"
echo "legs:".$kodok->legs."<br>";// 4
echo "cold_blooded:".$kodok->cold_blooded."<br>";// "no"
echo "jump:".$kodok->jump()."<br>";


echo "<br>";



// index.php
$sungokong = new Ape("kera sakti");
echo "name:".$sungokong->name."<br>"; // "buduk"
echo "legs:".$sungokong->legs."<br>";// 4
echo "cold_blooded:".$sungokong->cold_blooded."<br>";// "no"
echo "yell:".$sungokong->yell(); // "Auooo"




?>